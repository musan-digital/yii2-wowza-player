<?php
/**
 * Poject: yii2-wowza-player
 * User: mitrii
 * Date: 2.10.2017
 * Time: 15:53
 * Original File Name: WowzaPlayerAsset.php
 */

namespace lafa\widgets;


class WowzaPlayerAsset extends \yii\web\AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [];
    public $js = [
        '//player.wowza.com/player/latest/wowzaplayer.min.js',
    ];
    public $depends = [];
}