<?php

namespace lafa\widgets;

use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

class WowzaPlayer extends \yii\base\Widget
{
    public $options = [];

    public $htmlOptions = [];

    public $name = 'online';

    public function getPlayerOptions()
    {
        $this->options['license'] = $this->options['license'] ?? '';
        $this->options['title'] = $this->options['title'] ?? '';
        $this->options['description'] = $this->options['description'] ?? '';
        $this->options['sourceURL'] = $this->options['sourceURL'] ?? '';
        $this->options['autoPlay'] = $this->options['autoPlay'] ?? true;
        $this->options['volume'] = $this->options['volume'] ?? 60;
        $this->options['mute'] = $this->options['mute'] ?? false;
        $this->options['loop'] = $this->options['loop'] ?? false;
        $this->options['audioOnly'] = $this->options['audioOnly'] ?? false;
        $this->options['uiShowQuickRewind'] = $this->options['uiShowQuickRewind'] ?? false;
        $this->options['uiQuickRewindSeconds'] = $this->options['uiQuickRewindSeconds'] ?? 30;

        return Json::encode($this->options);
    }

    public function init()
    {
        parent::init();

        $this->htmlOptions['id'] = $this->id;
        $this->htmlOptions['style'] = $this->htmlOptions['style'] ?? 'width:100%; height:0; padding:0 0 56.25% 0';

        $this->registerAssetBundle();
        $this->registerJs();
    }


    public function run()
    {
        echo Html::tag('div', '', $this->htmlOptions);
    }

    public function registerAssetBundle()
    {
        WowzaPlayerAsset::register($this->getView());
    }

    public function registerJs()
    {
        $this->getView()->registerJs("WowzaPlayer.create('{$this->id}', {$this->playerOptions});", View::POS_READY);
        $this->getView()->registerJs("var {$this->name} = '{$this->id}';", View::POS_END);

    }
}